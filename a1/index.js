
  for (num = 100; num >= 0; num--) {
  	
  	if (num <= 50) {
  		console.log("50 break");
  		break
  	}

  	if (num % 10 == 0) {
  		console.log(num + " div by 10, skipping...");
  		continue;
  	}

  	if (num % 5 == 0) {
  		console.log(num);
  	}
  }

  let word = "supercalifragilisticexpialidocious";
  let consonants = "";

  for (let i = 0; i < word.length; i++){
  	
  	if (
  			word[i].toLowerCase() == "a" ||
  			word[i].toLowerCase() == "e" ||
  			word[i].toLowerCase() == "i" ||
  			word[i].toLowerCase() == "o" ||
  			word[i].toLowerCase() == "u"
  		) {
  		
  		continue

  	} else {
  		
  		console.log(word[i]);
  		console.log(consonants);
  		consonants = consonants.concat(word[i]);
  	}
  	
  }

  console.log(consonants);